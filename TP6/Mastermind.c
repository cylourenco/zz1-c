#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

SDL_Color WHITE = {255, 255, 255, 255};
SDL_Color COLOR = {0, 22, 0, 200};

SDL_Window   * window;
SDL_Renderer *renderer;
SDL_Texture *text;

void afficher_ecran(void)
{
    SDL_Rect rect;
    SDL_Event event;

    while (SDL_PollEvent(&event))
    {
        /* couleur de fond */
        SDL_SetRenderDrawColor(renderer, 0, 22, 0, 200);
        SDL_RenderClear(renderer);

        /* dessiner en blanc */
        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
        rect.x = rect.y = 0;
        rect.w = rect.h = 400;
        SDL_RenderFillRect(renderer, &rect );

        /* afficher à l'ecran */
        SDL_RenderPresent(renderer);

                
        rect.x = 600;
        rect.y = 110;
        rect.w = rect.h = 128;
        SDL_RenderCopy(renderer, avatar, NULL, &rect); 

    }
    SDL_Delay(1);
}

int main()
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
        return EXIT_FAILURE; 
    }

    //créer une fenêtre
    int width = 800;
    int height = 800;

    window = SDL_CreateWindow("SDL2 Programme 0.1", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 
                width, height, 
                SDL_WINDOW_RESIZABLE); 
        
    if (window == 0) 
    {
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
        /* on peut aussi utiliser SLD_Log() */
    }


    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED ); /*  SDL_RENDERER_SOFTWARE */
    if (renderer == 0) {
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); 
        /* faire ce qu'il faut pour quitter proprement */
    }

    int flags=IMG_INIT_JPG|IMG_INIT_PNG;
    int initted= 0;

    initted = IMG_Init(flags);

    if((initted&flags) != flags) 
    {
        printf("IMG_Init: Impossible d'initialiser le support des formats JPG et PNG requis!\n");
        printf("IMG_Init: %s\n", IMG_GetError());
    }

    SDL_Texture  *avatar;
    SDL_Rect rect;

    SDL_Surface *image = NULL;
    image=IMG_Load("loic.png");
    /* image=SDL_LoadBMP("loic.bmp"); fonction standard de la SDL2 */
    if(!image) {
        printf("IMG_Load: %s\n", IMG_GetError());
    }

    avatar = SDL_CreateTextureFromSurface(renderer, image);
    SDL_FreeSurface(image);



    SDL_Event event;
    int running = 1;

    while (running) {

        while (SDL_PollEvent(&event))
        {
            switch(event.type)
            {
                case SDL_WINDOWEVENT:
                    printf("window event\n");
                    switch (event.window.event)  
                    {
                        case SDL_WINDOWEVENT_CLOSE:
                            printf("appui sur la croix\n");	
                            break;
                        case SDL_WINDOWEVENT_SIZE_CHANGED:
                            width = event.window.data1;
                            height = event.window.data2;
                            printf("Size : %d%d\n", width, height);
                        default:
                            afficher_ecran(renderer, avatar);
                    }   
                    break;
                case SDL_MOUSEBUTTONDOWN:
                    printf("Appui :%d %d\n", event.button.x, event.button.y);
                    afficher_ecran(renderer, avatar);
                    break;
                case SDL_QUIT : 
                    printf("on quitte\n");    
                    running = 0;
            }
        }
        SDL_Delay(1);
	
    }

   // SDL_Delay(5000);
   SDL_DestroyTexture(avatar);
    IMG_Quit();
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

    SDL_Quit();
    return 0;
}