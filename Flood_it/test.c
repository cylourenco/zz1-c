#include "game.h"

int main()
{
    int **  grille  = initTab(),
            avant, 
            apres, 
            i       = 0, 
            j       = 0, 
            compteur= 0;
    
    initialiser(grille);
    afficherGrille(grille);

    printf("Choisissez une celulle de commencement: \n");
    scanf("%d", &i);
    scanf("%d", &j);

    printf("Couleur de la cellule de commencement: %d\n\n", grille[i][j]);

    avant = apres = grille[i][j];

    while (fin(grille) == 0 && compteur < 23)
    {
        printf("Choisissez une nouvelle couleur adjacente : ");
        scanf("%d", &apres);
        
        remplir(grille, i, j, avant, apres);
        afficherGrille(grille);

        avant = apres;
        compteur ++;
    }

    return 0;
}