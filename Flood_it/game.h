#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int ** initTab();

void initialiser(int ** tab);

void afficherGrille(int ** grille);

int fin(int ** grille);

int valide(int i, int j);

void remplir(int ** grille, int i, int j, int avant, int apres);
