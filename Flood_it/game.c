#include "game.h"

const int COULEURS = 6;
const int TAILLE = 12;

int ** initTab() 
{
    int ** t = malloc(TAILLE * sizeof(int*));

    if (t != NULL) 
    {
        for (int i = 0; i < TAILLE; i++) 
        { 
            t[i] = malloc(TAILLE * sizeof(int)); 
        }
    }

    return t;
}

void initialiser(int ** tab)
{
    int i, j;

    for (i = 0; i < TAILLE; i ++)
    {
        for (j = 0; j < TAILLE; j ++)
        {
            tab[i][j] = rand() % (COULEURS + 1);
        }
    }

}

void afficherGrille(int ** grille)
{
    int i, j;
    
    printf("\n---------------------GRILLE---------------------\n");

    for (i = 0; i < TAILLE; i ++)
    {
        printf("|");

        for (j = 0; j < TAILLE; j ++)
        {
            printf(" %d |", grille[i][j]);
        }

        printf("\n");
    }
    printf("\n");
}

int fin(int ** grille)
{
    int i, j, c = grille[0][0];

    for (i = 0; i < TAILLE; i ++)
    {
        for (j = 0; j < TAILLE; j ++)
        {
            if (c != grille[i][j])
            {
                return 0;
            }
        }
    }

    return 1;
}

int valide(int i, int j)
{
    if (i >= 0 && i < TAILLE && j >= 0 && j < TAILLE )
    {
        return 1;
    }
    return 0;
}

void remplir(int ** grille, int i, int j, int avant, int apres)
{
    if (valide(i, j) == 1)
    {
        if (grille[i][j] == avant)
        {
            grille[i][j] = apres;

            remplir(grille, i - 1, j, avant, apres);
            remplir(grille, i + 1, j, avant, apres);
            remplir(grille, i, j - 1, avant, apres);
            remplir(grille, i, j + 1, avant, apres);
        }
    }
}