#include "main.h"

void affiche_liste(liste_t * l)
{
    cellule_t * courant = l -> tete;
    int i = 0;
    
    if(courant == NULL)
    {
        printf("La liste est vide !\n");
    }
    while (courant != NULL)
    {
        printf("%d : %s\n", i, courant -> ligne);
        courant = courant -> suiv;
        i++;
    }
}

liste_t * inserer_fin(liste_t * l, cellule_t * nv)
{
    if(l -> fin == NULL || l -> tete == NULL)
    {
        nv -> suiv = NULL;
        l -> tete = nv;
        l -> fin = nv;

        return l;
    }
    else
    {
        l -> fin -> suiv = nv;
        l -> fin = nv;
        nv -> suiv = NULL;

        return l;
    }
}

liste_t * inserer_debut(liste_t * l, cellule_t * nv)
{
    if(l -> fin == NULL || l -> tete == NULL)
    {
        nv -> suiv = NULL;
        l -> tete = nv;
        l -> fin = nv;

        return l;
    }
    else
    {
        cellule_t * tmp = l -> tete;
        l -> tete = nv;
        nv -> suiv = tmp;

        return l;
    }
}

char * enleverSautLigne(char * saisie)
{
   saisie[ strlen(saisie) - 1 ] = '\0';
   return saisie;
}

cellule_t * creation_cellule_saisie(void)
{
    printf("Chaine de caractere de moins de 255 caracteres : \n");

    cellule_t * nv_l = (cellule_t *)malloc(sizeof(cellule_t));
    char * buffer = (char *) malloc(255 * sizeof(char)); 
    char quit[5] = "quit";

    while (fgets(buffer, 255, stdin) && enleverSautLigne(buffer) && strcmp(buffer, quit) != 0)
    {
        
        strcpy(nv_l -> ligne, buffer);
    }

    nv_l -> suiv = NULL;

    return nv_l;
}

liste_t * creation_cellule_fichier(void)
{
    FILE * fichier;
    fichier = fopen("text.txt", "r");
    int i;
    liste_t * l = liste_vide();

    char chaine[20] = "";

    if (fichier)
    {
	
        while(fgets(chaine, 20, fichier) != NULL)
        {
            enleverSautLigne(chaine);
            l = fichierLC(l, chaine);    
        }    
        fclose(file);
    }

    return l;
}

liste_t * fichierLC(liste_t * l, char * chaine)
{
    cellule_t * c = (cellule_t *)malloc(sizeof(cellule_t));
    strcpy(c -> ligne, chaine);
    l = inserer_debut(l, c);
    return l;
}

liste_t * liste_vide(void)
{
    liste_t * nv_l = (liste_t *)malloc(sizeof(liste_t));
    nv_l -> tete = NULL;
    nv_l -> fin = NULL;
    return nv_l;
}

int main(int argc, char  * argv[])
{
    liste_t * liste = liste_vide();
    liste = inserer_fin( liste, creation_cellule_saisie() );
    affiche_liste(liste);
}