#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct cellule 
{
  char             ligne[255];
  struct cellule * suiv;
} cellule_t;

typedef struct liste 
{
  cellule_t * tete;
  cellule_t * fin;
} liste_t;

void affiche_liste(liste_t * l);

liste_t * inserer_fin(liste_t * l, cellule_t * nv);

cellule_t * creation_cellule_saisie(void);

char * enleverSautLigne(char * saisie);

liste_t * liste_vide(void);