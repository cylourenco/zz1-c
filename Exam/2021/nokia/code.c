#include "code.h"
#include <stdio.h>
#include <stdlib.h>

char devinette[255] = "VOUS DEVEZ TROUVER";


void majuscules(char * s) {
   int i;
   i = 0;
   while ( s[i] != '\0' )
   {
      if (s[i] >= 97 && s[i] <= 122)
      {
         s[i] -= 32;
      }
      i ++;
   }
}

void multitap1(char * d, char * s) {
   int i, j;
   i = j = 0;
   majuscules(s);

   while (s[i] != '\0')
   {
      switch (s[i])
      {
      case 'A':
         d[j] = '2';
         d[j + 1] = ' ';
         j += 2;
         break;

      case 'B':
         d[j] = d[j + 1] = '2';
         d[j + 2] = ' ';
         j += 3;
         break;

      case 'C':
         d[j] = d[j + 1] = d[j + 2] = '2';
         d[j + 3] = ' ';
         j += 4;
         break;

      case 'D':
         d[j] = '3';
         d[j + 1] = ' ';
         j += 2;
         break;

      case 'E':
         d[j] = d[j + 1] = '3';
         d[j + 2] = ' ';
         j += 3;
         break;

      case 'F':
         d[j] = d[j + 1] = d[j + 2] = '3';
         d[j + 3] = ' ';
         j += 4;
         break;

      case 'G':
         d[j] = '4';
         d[j + 1] = ' ';
         j += 2;
         break;

      case 'H':
         d[j] = d[j + 1] = '4';
         d[j + 3] = ' ';
         j += 4;
         break;

      case 'I':
         d[j] = d[j + 1] = d[j + 2] = '4';
         d[j + 2] = ' ';
         j += 3;
         break;

      case 'J':
         d[j] = '5';
         d[j + 1] = ' ';
         j += 2;
         break;

      case 'K':
         d[j] = d[j + 1] = '5';
         d[j + 2] = ' ';
         j += 3;
         break;

      case 'L':
         d[j] = d[j + 1] = d[j + 2] = '5';
         d[j + 3] = ' ';
         j += 4;
         break;

      case 'M':
         d[j] = '6';
         d[j + 1] = ' ';
         j += 2;
         break;

      case 'N':
         d[j] = d[j + 1] = '6';
         d[j + 2] = ' ';
         j += 3;
         break;

      case 'O':
         d[j] = d[j + 1] = d[j + 2] = '6';
         d[j + 3] = ' ';
         j += 4;
         break;

      case 'P':
         d[j] = '7';
         d[j + 1] = ' ';
         j += 2;
         break;

      case 'Q':
         d[j] = d[j + 1] = '7';
         d[j + 2] = ' ';
         j += 3;
         break;

      case 'R':
         d[j] = d[j + 1] = d[j + 2] = '7';
         d[j + 3] = ' ';
         j += 4;
         break;

      case 'S':
         d[j] = d[j + 1] = d[j + 2] = d[j + 3] = '7';
         d[j + 4] = ' ';
         j += 5;
         break;

      case 'T':
         d[j] = 8;
         d[j + 1] = ' ';
         j += 2;
         break;

      case 'U':
         d[j] = d[j + 1] = '8';
         d[j + 2] = ' ';
         j += 3;
         break;

      case 'V':
         d[j] = d[j + 1] = d[j + 2] = '8';
         d[j + 3] = ' ';
         j += 4;
         break;

      case 'W':
         d[j] = '9';
         d[j + 1] = ' ';
         j += 2;
         break;

      case 'X':
         d[j] = d[j + 1] = '9';
         d[j + 2] = ' ';
         j += 3;
         break;

      case 'Y':
         d[j] = d[j + 1] = d[j + 2] = '9';
         d[j + 3] = ' ';
         j += 4;
         break;

      case 'Z':
         d[j] = d[j + 1] = d[j + 2] = d[j + 3] = '9';
         d[j + 4] = ' ';
         j += 5;
         break;
      
      default:
         d[j] = s[i];
         j ++;
         break;
      }
      i ++;
   }
   d[j - 1] = '\0';
}

void multitap2(char * s) {
   
}

void antitap1(char *d, char * s) {
}

void antitap2(char * s) {
}

char * sms(char * nom) {

   FILE * fichier = NULL;
   fichier = fopen(nom, "r");

   char  * sortie = NULL, 
         * entree = NULL;

   if (fichier)
   {
      sortie = malloc(100 * sizeof(char));
      entree = malloc(100 * sizeof(char));

      int i;
      i = 0;

      do
      {
         entree[i] = fgetc(fichier);
         printf("%c", entree[i]);
         i ++;
      }while (entree[i] != EOF);
      
      multitap1(sortie, entree);
   }
   fclose(fichier);
   return sortie;
}