#include "code.h"
#include <stdio.h>
#include <stdlib.h>

char * init(char * tab, int taille) {
   tab = malloc(taille * sizeof(char)); // à mettre si allouer dynamiquement sinon mettre en commentaire
   int i;
   for ( i = 0; i < taille; i ++)
   {
      if (i == 0 || i == 1)
      {
         tab[i] = 0;
      }
      else
      {
         tab[i] = 1;
      }
   }
   return tab;
}  

void affichage(FILE * file, char * tab, int taille ) {
   if (file != NULL)
   {
      int i;
      for (i = 0; i < taille; i ++)
      {
         if (tab[i] == 1)
         {
            fprintf(file, "%d ", i);
            printf("%d ", i);
         }
      }
   }
}

void suppMultiple(char * tab, int taille, int nb) {
   int i;
   for (i = 0; i < taille; i ++)
   {
      if (i % nb == 0 && i != nb)
         tab[i] = 0;
   }
}

int prochainNombre(char * tab, int taille, int nb) {
   int i;
   i = 0;
   while ( i != taille - 1)
   {
      if (tab[i] == 1)
         return i;
      i ++;
   }
   return -1;
}

int listeNombrePremiers(FILE * file, int taille) {
   int courant, sortie = 1;

   int * tab = NULL;
         tab = init(tab, taille);
         courant = prochainNombre(tab, taille);

   while (courant != -1)
   {
      suppMultiple(tab, taille, courant);
      courant = prochainNombre(tab, taille);
      sortie ++;
   }
   return sortie;
}

