#include <stdlib.h>
#include <stdio.h>

typedef struct {
    int val ;
    struct Arbre * sag ;// Sous arbre gauche
    struct Arbre * sad ;// sous arbre droit
} arbre ;
typedef struct {

int nb_elem(arbre *a){
    if(a == NULL){ return 0; }
    return(nb_elem(a->sag) + nb_elem(a->sad) + 1);
}

int affiche_infixe(arbre *a){
    if(a == NULL){ return 0; }
    if(a->sag !== NULL) { affiche_infixe(a->sag); }
    printf("%d", a->val);
    if(a->sad !== NULL) { affiche_infixe(a->sad); }
}

int affiche_prefixe(arbre *a){
    if(a == NULL){ return 0; }
    printf("%d", a->val);
    if(a->sag !== NULL) { affiche_prefixe(a->sag); }
    if(a->sad !== NULL) { affiche_prefixe(a->sad); }
}

int affiche_sufixe(arbre *a){
    if(a == NULL){ return 0; }
    if(a->sag !== NULL) { affiche_sufixe(a->sag); }
    if(a->sad !== NULL) { affiche_sufixe(a->sad); }
    printf("%d", a->val);
}

void tab(arbre *a){
    int * t = malloc(nb_elem(*int));
    int i=0;
    while(a != NULL){
        if(a->sag !== NULL) { affiche_infixe(a->sag); }
        t[i] a->val);
        if(a->sad !== NULL) { affiche_infixe(a->sad); }
        i++;
    }
}

int main() {
    return 0;
}