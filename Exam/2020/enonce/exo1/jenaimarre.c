#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

char * init(char * tab, int taille) {
   tab = malloc(taille * sizeof(char)); // à mettre si allouer dynamiquement sinon mettre en commentaire
   int i;
   for ( i = 0; i < taille; i ++)
   {
      if (i == 0 || i == 1)
      {
         tab[i] = 0;
      }
      else
      {
         tab[i] = 1;
      }
   }
   return tab;
}  

void affichage(char * tab, int taille ) {
   if (1)
   {
      int i;
      for (i = 0; i < taille; i ++)
      {
         if (tab[i] == 1)
         {
            printf("%d ", i);
         }
      }
      printf("\n");
   }
}

void suppMultiple(char * tab, int taille, int nb) {
   int i;
   for (i = 0; i < taille; i ++)
   {
      if (i % nb == 0 && i != nb)
         tab[i] = 0;
   }
}

int prochainNombre(char * tab, int taille, int nb) {
   int i;
   i = nb;
   i++;
   while ( i < taille)
   {
      if (tab[i] == 1)
         return i;
      i ++;
   }
   return -1;
}

int listeNombrePremiers(int taille) {
   int courant, sortie = 0;

   char * tab = NULL;
         tab = init(tab, taille);
         courant = prochainNombre(tab, taille, 0);

   while (courant != -1)
   {
        printf("%d ", courant);
      suppMultiple(tab, taille, courant);
      courant = prochainNombre(tab, taille, courant);
      sortie ++;
   }
   return sortie;
}

int main()
{
    char * tab;
    int taille = 20;
    tab = init(tab, taille);
    affichage(tab, taille);
    suppMultiple(tab, taille, 2);
    affichage(tab, taille);
    suppMultiple(tab, taille, 3);
    affichage(tab, taille);
    printf("%d\n", prochainNombre(tab, taille, 2));
    printf("%d\n", prochainNombre(tab, taille, 3));
    printf("%d\n", prochainNombre(tab, taille, 4));
    printf("%d\n", prochainNombre(tab, taille, 5));
    printf("%d\n", prochainNombre(tab, taille, 7));
    printf("%d\n", prochainNombre(tab, taille, 11));
    printf("%d\n", prochainNombre(tab, taille, 13));
    printf("%d\n", prochainNombre(tab, taille, 15));
    printf("%d\n", prochainNombre(tab, taille, 17));
    printf("%d\n", prochainNombre(tab, taille, 19));
    printf("%d\n", prochainNombre(tab, taille, 30));
    printf("\nnombre : %d\n", listeNombrePremiers(10));
    printf("\nnombre : %d\n", listeNombrePremiers(20));
    printf("\nnombre : %d\n", listeNombrePremiers(100));
    return 0;
}