#include "code.h"
#include <math.h>
#include <string.h>
#include <stdlib.h>

int convert_to_d(char * s) {
   int   i, dec, tmp, cpt;
         i = 0;
         dec = 0;
         cpt = 0;

   while(s[i] != '\0')
   {
      cpt ++;
      i ++;
   }

   i = 0;
   while(cpt > 0)
   {
      if(s[i] > 64)
      {
         tmp = s[i] - 55; 
      }
      else
      {
         tmp = s[i] - 48;
      }

      dec += tmp * pow(16, cpt-1);
      i ++;
      cpt --;
   }
   
   return dec;
}

// ********************************************************

int is_prime(int n)
{
   int i;
   if (n < 2)
   {
      return 0;
   }
   int racine = sqrt(n);

   for (i = 2; i <= racine; i ++)
   {
      if (n % i == 0)
      {
         return 0;
      }
   }
   return 1;
}

void facteurs_simple(char * res, int n) {
   int a = 2;
   char tmp[5];
   char r[255] = "";
   while (n != 1)
   {
      while (n % a == 0)
      {
         n /= a;
         sprintf(tmp, "%d ", a);
         strcat(r, tmp);
      }
      a ++;
   }
   r[strlen(r) - 1] = '\0';
   strcpy(res, r);
}

void facteurs(char * res, int n) {
   int a = 2;
   int i;
   char tmp[5];
   char r[255] = "";
   while (n != 1)
   {
      i = 0;

      while (n % a == 0)
      {
         n /= a;
         i ++;
      }

      if (i > 1)
      {
         sprintf(tmp, "%d^", a);
         strcat(r, tmp);
         sprintf(tmp, "%d*", i);
         strcat(r, tmp);
      }

      else if (i == 1)
      {
         sprintf(tmp, "%d*", a);
         strcat(r, tmp);
      }

      a ++;
   }
   r[strlen(r) - 1] = '\0';
   strcpy(res, r);
   printf("%s\n", res);
}


// ********************************************************

double ** creer_id(int n)  {
   int i , j;
   double ** mat = malloc(n * sizeof(double*));
   for (i = 0; i < n; i ++ )
   {
      mat[i] = malloc(n * sizeof(double));
   }
   for (i = 0; i < n; i ++ )
   {
      for (j = 0; j < n; j ++ )
      {
         if (i == j)
         {
            mat[i][j] = 1;
         }
         else
         {
            mat[i][j] = 0;
         }
      }
   }
   return mat;
}

void liberer(double **m, int n) {
   int i;
   for (i = 0; i < n; i ++ )
   {
      free(m[i]);
   }
   free(m);;
}

double ** lire_mat(char * nom, int * pordre) {
   FILE * fichier = fopen(nom, "r");
   double ** mat;
   
   if (fichier) {

		fscanf(fichier, "%d", pordre);
		
      mat = creer_id(*pordre);

      for (int i = 0; i < *pordre; i++)
      {
         for (int j = 0; j < *pordre; j++)
         {
               fscanf(fichier, "%lf", &mat[i][j]); 
         }
      }
   }
		
   fclose(fichier);
   return mat;
}
