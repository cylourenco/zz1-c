#include "code.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


void stats1(char *texte, int *nb_chars, int * nb_lignes, int * nb_mots)
{
  int carac, lignes, mots, i;
  i = carac = mots = lignes = 0;

  while (texte[i] != '\0')
  {
    if (texte[i] == '\n')
    {
      lignes ++;
      mots ++;
    }
    if (texte[i] == ' ' )
    {
      mots ++;
    }
    if (texte[i + 1] == '\0')
    {
      if (texte[i] != '\n')
      {
        mots ++;
      }
      else
      {
        mots --;
      }
        
    }
    carac ++;
    i ++;
  }
  lignes ++;
  
  *nb_chars = carac;
  *nb_lignes = lignes;
  *nb_mots = mots;
}  


long taille_fichier(char * nom) {
  FILE * f  = fopen(nom, "rb");
  long r = 0;
  if (f) {
    fseek(f, 0, SEEK_END);
    r = ftell(f);
    fclose(f);
  }
  return r;
}

char * lecture(char * nom) {
  FILE * f = fopen(nom, "r");
  char * retour = NULL;
  if (f)
  {
    retour = malloc (taille_fichier(nom) * sizeof (char));
    char * buffer = malloc (taille_fichier(nom) * sizeof (char));

    while (fgets(buffer, taille_fichier(nom), f) != NULL)
    {
      strcat(retour, buffer);
    }
  }

  printf("%s", retour);
  fclose(f);

  return retour;
} 

void initialiser( info * infos) 
{
  int i;
  for (i = 0; i < 26; i ++)
  {
    infos[i].lettre = 'A' + i;
    infos[i].nb = 0;
    infos[i].nbd = 0;
  }
  for (i = 0; i < MAX; i ++)
  {
    strcpy(infos[25].mots[i], "");
  }
}

void inserer(info * infos, char * mot )
{
  int i, j, k, l, verif;
  i = j = verif = 0;

  while (infos[i].lettre != mot[0])
  {
    i++;
  }

  infos[i].nb ++;

  while (0 != strcmp(infos[i].mots[j], ""))
  {
    if (0 == strcmp(infos[i].mots[j], mot))
    {
      verif = -1;
    }
    j ++;
  }

  if (verif != -1)
  {
    strcpy(infos[i].mots[j], mot);
    infos[i].nbd ++;
    for (k = 0; k < j + 1; k++) 
    {
      for (l = 0; l < j + 1; l++)
      {
        if (strcmp(infos[i].mots[k], infos[i].mots[l]) < 0)
        {
          char temp[50];
          strcpy(temp, infos[i].mots[k]);
          strcpy(infos[i].mots[k], infos[i].mots[l]);
          strcpy(infos[i].mots[l], temp);
        }
      }
    }
  }

} 

void stats2(char *texte, info * infos, int *nb_chars, int * nb_lignes, int * nb_mots)
{
  stats1(texte, &nb_chars, &nb_lignes, &nb_mots);
  int i = 0;

  char tmp[50];

  while (texte[i] != '\0')
  {

     if (texte[i] == '\n' || texte[i] == ' ' )
     {
        printf("%s\n", tmp);
        inserer(infos, tmp);
        strcpy(tmp, ""); 
     }
     else{
        strncat(tmp, &texte[i], 1); 
     }

     i ++;
  }

    
}   

int total_mots(info * infos ) {
  int i, resultat = 0; 
  for(i = 0; i < 26; i ++)
  {
    resultat += infos[i].nb;
  }
  return resultat;
}

int total_mots_distincts(info * infos) {
  int i, resultat = 0; 
  for(i = 0; i < 26; i ++)
  {
    resultat += infos[i].nbd;
  }
  return resultat;
}
