#include "code.h"

#include<stdio.h>
#include <math.h>
#include <stdlib.h>

void init(char * tab, int taille, char value) 
{
	int i;
    for(i = 0; i < taille; i ++)
    {
        tab[i] = value;
    }
    tab[taille] = '\0';
}  

void inverser(char * tab) 
{
    int i,
        taille = 0;
    char tmp;

    while (tab[taille] != '\0')
    {
        taille ++;
    }
    
    for (i = 0; i < (taille / 2) ; i ++)
    {
        tmp = tab[i];
        tab[i] = tab[taille - i - 1];
        tab[taille - i -1] = tmp;
    }
}

void conversiond2b(int nb, char * tab) 
{
    int i = 0, 
        reste;

    if (nb == 0)
    {
        tab[i] = '0';
        i ++;
    }

    while (nb != 0)
    {
        if (nb % 2 != 0)
            tab[i] = '1';
        else
            tab[i] = '0';

        nb /= 2;
        i ++;
    }
    tab[i] = '\0';
    inverser(tab);
}

int conversionb2d(const char * tab)
{
	int i, a, puiss,
        taille  = 0,
        nb      = 0;
    
    while (tab[taille] != '\0')
    {
        taille ++;
    }

    puiss = taille - 1;

    for (i = 0; i < taille; i ++)
    {
        if (tab[i] == '1')
        {
            nb += pow(2, puiss);
        }

        puiss --;
    }
    
    return nb;

}

char * addition(char *r, char *a, char * b) {
    int a_dec, b_dec, r_dec;

    a_dec = conversionb2d(a);
    b_dec = conversionb2d(b);
    r_dec = a_dec + b_dec;
    
    conversiond2b(r_dec, r);

    return r;
}	

