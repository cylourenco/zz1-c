#ifndef _histo_h_
#define _histo_h_
#include "liste.h"

#define HISTOSIZE 21

typedef int histogram_t[HISTOSIZE];

typedef struct gdata_s {
    Window         root;       
    Window         win;           
    Display       *dpy;    
    int            ecran;        
    GC             gcontext;         
    XGCValues      gcv;              
    Visual        *visual;
    Colormap       colormap;
    Font           font;
} gdata_t;

void computeHisto(histogram_t h, list_t l);

void displayHisto(histogram_t h);

int maxHisto(histogram_t h);

float meanHisto(histogram_t h);

int countHisto(histogram_t h);

void displayGraphicalHisto(gdata_t g, histogram_t h);

void displayGraph(histogram_t h);

void displayText(histogram_t h);

#endif