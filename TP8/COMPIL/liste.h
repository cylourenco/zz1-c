#ifndef _liste_h_
#define _liste_h_

extern int ERROR;

typedef struct cell_s {
    char key[30];
    int  value;
    struct cell_s * nk, *nv;
} cell_t;

typedef struct list_s {
    cell_t * key; // pointe sur la 1ère cellule clef
    cell_t * value; // pointe sur la 1ère cellule valeur
} list_t;

void initList(list_t * plist);

int insert(list_t * plist, char * nom, int note);

void displayByKey(list_t list);

void displayByValue(list_t list);

void freeList(list_t *plist);

#endif

