#include "calculatrice.h"

const char * OPER_NAMES[] = { "x", "sin(x)", "cos(x)", "log(x)", "exp(x)", NULL };

double identite(double x){
    return x;
}

double erreur(double x){
    fprintf(stderr,"mauvaise fonction appelée");
    return -1;
}

double (*OPER_FN [])(double) = { identite, sin, cos, log, exp, erreur };

void get_rid_of_chariot(char * chaine)
{
    char c = chaine[0];
    int i = 0;
    while (c != '\0' && c!= '\n')
    {
        i++;
        c = chaine[i];
    }
    if(chaine[i] == '\n')
        chaine[i] = '\0';
    }

/*
int identification(char * str)
{
    int i;
    i = 0;
    while (OPER_NAMES[i] != NULL)
    {
        if (strcmp(str, OPER_NAMES[i]) == 0) return i;
        i ++;
    }
    return -1;
}
*/

OP identification(char * str)
{
    int i;
    i = 0;
    while (OPER_NAMES[i] != NULL)
    {
        if (strcmp(str, OPER_NAMES[i]) == 0)
        {
            switch(i)
            {
                case 0:
                    return ID;
                case 1:
                    return SIN;
                case 2:
                    return COS;
                case 3:
                    return LOG;
                default:
                    return EXP;
            }
        }
        i ++;
    }
    return NONE;
}

double evalf(double val, OP o)
{
    switch(o)
    {
        case ID:
            return val;
        case SIN:
            return sin(val);
        case COS:
            return cos(val);
        case LOG:
            return log(val);
        case EXP:
            return exp(val);
        default:
            return -1;
    }
}

void calcul(double a, double b, double delta, OP o, FILE * fichier)
{
    double i = a;
    while (i < b) 
    {
        fprintf(fichier, "%lf\n", evalp(i, o));
        i = i + delta;
    }
}

double evalp(double val, OP o)
{
    return OPER_FN[o](val);
}



