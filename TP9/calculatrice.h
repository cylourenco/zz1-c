#ifndef calc_h
#define calc_h

#include <stdio.h>
#include <string.h>
#include <math.h>

typedef enum ope {
    NONE = -1, ID , SIN, COS, LOG, EXP
} OP;


void get_rid_of_chariot(char *);

//int identification(char * str);
OP identification(char * str);

double evalf(double val, OP o);

void calcul(double a, double b, double delta, OP o, FILE * fichier);

double evalp(double val, OP o);






#endif