#include "teZZt.h"
#include "calculatrice.h"

// BEGIN_TEST_GROUP(test)

// TEST(identification_avec_indice) {
//   CHECK(  0 == identification("x"));
//   CHECK( -1 == identification("x\n"));
//   CHECK(  1 == identification("sin(x)"));
//   CHECK(  2 == identification("cos(x)"));
//   CHECK(  3 == identification("log(x)"));
//   CHECK(  4 == identification("exp(x)"));
//   CHECK( -1 == identification("t"));
// }


// TEST(identification_avec_enum) {
//   CHECK(   ID == identification("x"));
//   CHECK( NONE == identification("x\n"));
//   CHECK(  SIN == identification("sin(x)"));
//   CHECK(  COS == identification("cos(x)"));
//   CHECK(  LOG == identification("log(x)"));
//   CHECK(  EXP == identification("exp(x)"));
//   CHECK( NONE == identification("t"));
// }

// TEST(control_eval_f) {
//    CHECK( EQ(0.0, evalf( 0.0, ID)));
//    CHECK( EQ(0.0, evalf( 0.0, SIN)));
//    CHECK( EQ(1.0, evalf( 0.0, COS)));
//    CHECK( EQ(0.0, evalf( 0.0, ID)));

//    CHECK( EQ(      M_PI , evalf( M_PI, ID)));
//    CHECK( EQ(       0.0 , evalf( M_PI, SIN)));
//    CHECK( EQ(      -1.0 , evalf( M_PI, COS)));
//    CHECK( EQ( exp(M_PI) , evalf( M_PI, EXP)));
   
//    CHECK( EQ( 0.0 , evalf( M_PI/2.0, COS)));
// }

// END_TEST_GROUP(test)




int main(){


    double a, b, delta;
    OP o = -1;
    char op[255] = "", stra[255] = "", strb[255] = "", strdelta[255] = "";

    printf("Donnez une opération :    [x , sin(x) , cos(x) , log(x) , exp(x)]\n");
    fgets(op, 255, stdin);
    get_rid_of_chariot(op);

    o = identification(op);

    puts("Donnez en une ligne et dans l'ordre suivant : borneA, borneB et Pas");
    fgets(stra, 255, stdin);
    get_rid_of_chariot(stra);
    a = atol(stra);

    fgets(strb, 255, stdin);
    get_rid_of_chariot(strb);
    b = atol(strb);

    fgets(strdelta, 255, stdin);
    get_rid_of_chariot(strdelta);
    delta = atol(strdelta);
    
    calcul(a, b, delta, o, stdout);
   
    


    //RUN_TEST_GROUP(test);

    return 0;
}