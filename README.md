# ZZ1-C

Les TPs de C de 1ère année à l'Isima.  
Ces TPs taitent des tableaux, des pointeurs, des structures, des listes, des Makefile, des union, de l'enum ainsi que de la SDL.

## Auteurs
* **Cynthia LOURENCO** _alias_ [@cylourenco](https://gitlab.isima.fr/users/cylourenco)


