#include <stdio.h>
#include <unistd.h>

void echangeParAdresse(int * a, int * b)
{
    int tmp;
    tmp = *a;
    *a  = *b;
    *b  = tmp;
}

void echangeParValeur(int a, int b)
{
    int tmp;
    tmp = a;
    a   = b;
    b   = tmp;
}

int main()
{
    int     i = 1 , *ptri  = &i;
    int     j = 0 , *ptrj  = &j;
    char    c1 = '1', *ptrc1 = &c1;
    char    c2 = '2', *ptrc2 = &c2;   
    char    tmp; 
    double  d, *ptrd = &d;

/*
    printf("ptri = %u ptrc1 = %u \n",*ptri, *ptrc1);
    printf("ptri = %x ptrc1 = %x \n",*ptri, *ptrc1);
*/    // seule la version suivante ne genere pas d'avertissement
 
    printf("Adresse contenu : ptri = %p ptrc1 = %p \n",ptri, ptrc1);
    printf("Valeur : *ptri = %d et *ptrc1=%c\n", *ptri, *ptrc1);
    
    printf("Size : ptri = %u ptrc1 = %d \n",*ptri, *ptrc1);
    printf("Valeur pointee ptrd : %lf\nAdresse contenue ptrd: %p\nAdresse du pointeur: %p\n", *ptrd, ptrd, &ptrd);

    // cela permet de voir la taille d'un int et d'un char en memoire
    // sizeof(int)  sizeof(char)

    d += 2;

    printf("Valeur pointee ptrd : %lf\nAdresse contenue ptrd: %p\nAdresse du pointeur: %p\n", *ptrd, ptrd, &ptrd);
    
    printf("Avant echange\nValeur pointee ptrc1 : %c\nValeur pointee ptrc2 : %c\n", *ptrc1, *ptrc2);
    
    tmp     = *ptrc1;
    *ptrc1  = *ptrc2;
    *ptrc2  = tmp;

    printf("Apres echange\nValeur pointee ptrc1 : %c\nValeur pointee ptrc2 : %c\n", *ptrc1, *ptrc2);

    printf("Avant echange\n");
    printf("i : %d | j : %d\n", i, j);

    echangeParValeur(i, j);
    printf("Apres echange par valeur\n");
    printf("i : %d | j : %d\n", i, j);

    echangeParAdresse(ptri, ptrj);
    printf("Apres echange par adresse\n");
    printf("i : %d | j : %d\n", *ptri, *ptrj);

    return 0 ;
}
