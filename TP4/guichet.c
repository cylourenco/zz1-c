#include <stdio.h>
#include <stdlib.h>
#include <time.h>


#define NB 12

int * tab[NB];
int guichet_A, guichet_B, guichet_C, guichet_D, guichet_E, guichet_F, guichet_G, guichet_H, guichet_I, guichet_J, guichet_K, guichet_L;

void fermeture_guichet()
{
    int i,
        n = (rand() % NB + 1) + 1;

    
    for (i = 0; i < NB; i ++)
    {
        if(i == n)
        {
            *tab[i] = 1;
        }
        else
        {
            *tab[i] = 0;
        }
    }
}

void afficher_tableau()
{
    int i;
    for (i = 0; i < NB; i ++)
    {
        printf("%d  ", *tab[i]);
    }  
    printf("\n");  
}

int main()
{
    srand( time( NULL ) );

    tab[0] = &guichet_A;
    tab[1] = &guichet_B;
    tab[2] = &guichet_C;
    tab[3] = &guichet_D;
    tab[4] = &guichet_E;
    tab[5] = &guichet_F;
    tab[6] = &guichet_G;
    tab[7] = &guichet_H;
    tab[8] = &guichet_I;
    tab[9] = &guichet_J;
    tab[10] = &guichet_K;
    tab[11] = &guichet_L;

    fermeture_guichet();
    afficher_tableau();
    
    return 0;
}