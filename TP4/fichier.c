#include <stdio.h>
#include <stdlib.h>

int main()
{
    FILE * fichier;
    float reel;
    char chaine[80];



    fichier = fopen("nom_du_fichier", "w+");
    if (fichier) 
    {
        printf("on peut lire le fichier\n");

        fprintf(fichier, "du texte %f", 10.1);

        rewind(fichier);
        fscanf(fichier, "%f", &reel);
        rewind(fichier);
        fgets(chaine, 80, fichier);
    } 

    fclose(fichier);
    printf("%c\n", chaine[5]);
    printf("%f\n", reel);
}